These are the programming assignments relative to Python for everybody - databases (4th part)

- Week 2: Basic Structured Query Language
- Week 3: Data Models and Relational SQL
- Week 4: Many-to-Many Relationships in SQL
- Week 5: Databases and Visualization

For more information, I invite you to have a look at https://www.coursera.org/learn/python-databases
